#include <cassert>
#include "TypeInfo.h"

CRegisteredTypeInfo::CRegisteredTypeInfo( const char* _name, const char* _definitionFile, int _definitionLine, int _id ) :
	name( _name ), definitionFile( _definitionFile ), definitionLine( _definitionLine ), id( _id )
{

}

std::string CRegisteredTypeInfo::GetName() const
{
	return name;
}

int CRegisteredTypeInfo::GetHashCode() const
{
	return id;
}

bool CRegisteredTypeInfo::Equals( const CRegisteredTypeInfo* other ) const
{
	return (this == other);
}

bool CRegisteredTypeInfo::CanBeUpcastedTo( const CRegisteredTypeInfo* upClass ) const
{
	if( upClass == this ) {
		return true;
	}
	for( size_t i = 0; i < parents.size(); i++ ) {
		if( parents[i]->CanBeUpcastedTo( upClass ) ) {
			return true;
		}
	}
	return false;
}

ptrdiff_t CRegisteredTypeInfo::UpcastTo( const CRegisteredTypeInfo* upClass, ptrdiff_t object ) const
{
	if( upClass == this ) {
		return object;
	}
	for( size_t i = 0; i < parents.size(); i++ ) {
		ptrdiff_t result = parents[i]->UpcastTo( upClass, object + shifts[i] );
		if( result != 0 ) {
			return result;
		}
	}
	return 0;

}

void CRegisteredTypeInfo::AddParent( const CRegisteredTypeInfo* parent, ptrdiff_t shift )
{
	for( size_t i = 0; i < parents.size(); i++ ) {
		if( parents[i] == parent ) {
			assert( shifts[i] == shift );
			return;
		}
	}
	parents.push_back( parent );
	shifts.push_back( shift );
}

//---------------------------------------------------------------------------------------------------------------------

std::map<std::string, int> __globalTypesMap;
std::vector<CRegisteredTypeInfo*> __globalTypesList;

int __GetTypeIdOrRegister( const char* name, const char* filename, int line )
{
	std::string searchName( name );
	searchName += "#";
	searchName += line;
	searchName += "#";
	searchName += filename;

	auto position = __globalTypesMap.find( searchName );
	if( position == __globalTypesMap.end() ) {
		CRegisteredTypeInfo* info = new CRegisteredTypeInfo( name, filename, line, __globalTypesList.size() );
		__globalTypesList.push_back( info );
		__globalTypesMap[searchName] = info->GetHashCode();
		return info->GetHashCode();
	} else {
		return position->second;
	}

	return 0;
}

CRegisteredTypeInfo* __GetTypeById( int id )
{
	return __globalTypesList[id];
}

CRegisteredTypeInfo * __GetTypeOrRegister( const char* name, const char* filename, int line )
{
	return __GetTypeById( __GetTypeIdOrRegister( name, filename, line ) );
}

const CRegisteredTypeInfo* __GetInternalTypeInfo( const CTypeInfo& value )
{
	return value.info;
}

void __AddDerived( const CRegisteredTypeInfo* child, const CRegisteredTypeInfo* parent, ptrdiff_t difference )
{
	const_cast<CRegisteredTypeInfo*>(child)->AddParent( parent, difference );
}

//---------------------------------------------------------------------------------------------------------------------

CTypeInfo::CTypeInfo( const CRegisteredTypeInfo* _info ) : info( _info )
{
}

CTypeInfo::CTypeInfo( const CTypeInfo& other ) : CTypeInfo( other.info )
{
}

std::string CTypeInfo::GetName() const
{
	return info->GetName();
}

int CTypeInfo::GetHashCode() const
{
	return info->GetHashCode();
}

bool CTypeInfo::Equals( const CTypeInfo& other ) const
{
	return info->Equals( other.info );
}

bool CTypeInfo::operator == ( const CTypeInfo& other )
{
	return Equals( other );
}

bool CTypeInfo::operator != ( const CTypeInfo& other )
{
	return !Equals( other );
}