#pragma once

#include "TypeInfo.h"

// ����� ��� ��������� ���������� � ����
class CObjectWithRTTI {
public:
	CObjectWithRTTI( const CRegisteredTypeInfo* type );
	CTypeInfo GetType() const;

private:
	const CRegisteredTypeInfo* GetInternalTypeInfo() const;
	const CRegisteredTypeInfo* type;

	template<typename TO, typename FROM>
	friend TO* DynamicCast( FROM* object );
};

#define OBJECT_WITH_RTTI_CTOR(T) CObjectWithRTTI(__GetInternalTypeInfo(TYPEID(T)))

//---------------------------------------------------------------------------------------------------------------------

ENABLE_RTTI( CObject );

// ������� ����� ��� �������, � ������� ������ �������� ���������� � ����
class CObject : virtual public CObjectWithRTTI {
public:
	CObject() : OBJECT_WITH_RTTI_CTOR( CObject )
	{
	}
};

//---------------------------------------------------------------------------------------------------------------------

// �������������� ����� ��� ���������� � ��������� ����������� ���������� (��� dynamic_cast)
template<typename TO, typename FROM>
TO* DynamicCast( FROM* object )
{
	const CRegisteredTypeInfo* toTypeInfo;
	try {
		toTypeInfo = __GetInternalTypeInfo( TYPEID( TO ) );
	}
	catch( std::logic_error& ex ) {
		return 0;
	}
	const CRegisteredTypeInfo* fromTypeInfo = object->GetInternalTypeInfo();
	ptrdiff_t result = fromTypeInfo->UpcastTo( toTypeInfo, reinterpret_cast<ptrdiff_t>(object) );
	if( result == 0 ) {
		return 0;
	} else {
		return reinterpret_cast<TO*>(result);
	}
}

#define DYNAMIC_CAST DynamicCast

//---------------------------------------------------------------------------------------------------------------------

template<>
class CTypeInfoGenerator<CObjectWithRTTI> : public CTypeInfo {
public:
	CTypeInfoGenerator( const CObjectWithRTTI& value ) : CTypeInfo( value.GetType() )
	{
	}
	CTypeInfoGenerator( const CObjectWithRTTI* value ) : CTypeInfo( value->GetType() )
	{
	}
};
