#include <cstdio>
#include <cassert>
#include <iostream>

#include "Rtti.h"

class CA {

};

class CD : public CA {

};

ENABLE_RTTI( CA );
ENABLE_RTTI( CD );
DERIVED( CD, CA );

ENABLE_RTTI( CObjA );
DERIVED( CObjA, CObject );

class CObjA : public CObject {
public:
	CObjA( int );

	int GetValue() { return value; }
private:
	int value;
};

CObjA::CObjA( int _value ) : OBJECT_WITH_RTTI_CTOR( CObjA ), value( _value )
{

}

ENABLE_RTTI( CObjB )
DERIVED( CObjB, CObjA );

class CObjB : public CObjA {
public:
	CObjB( int value ) : CObjA( value ), OBJECT_WITH_RTTI_CTOR( CObjB ) {}
};

ENABLE_RTTI( CObjC )
DERIVED( CObjC, CObjB )

class CObjC : public CObjB {
public:
	CObjC( int value ) : CObjB( value ), OBJECT_WITH_RTTI_CTOR( CObjC ) {}
};

ENABLE_RTTI( CObjD )
DERIVED( CObjD, CObjA )

class CObjD : public CObjA {
public:
	CObjD( int value ) : CObjA( value ), OBJECT_WITH_RTTI_CTOR( CObjD ) {}
};

ENABLE_RTTI( CObjF )
DERIVED( CObjF, CObject )

class CObjF : public CObject {
public:
	CObjF() : OBJECT_WITH_RTTI_CTOR( CObjF ) {}
};

ENABLE_RTTI( CObjE )
DERIVED( CObjE, CObjC )
DERIVED( CObjE, CObjF )

class CObjE : public CObjF, public CObjC {
public:
	CObjE( int value ) : CObjC( value ), OBJECT_WITH_RTTI_CTOR( CObjE ) {}
};


int main()
{
	std::cout << TYPEID( CA ).GetName() << std::endl;
	std::cout << TYPEID( CD ).GetName() << std::endl;
	CA ca;
	std::cout << GetTypeInfoValue( ca ).GetName() << std::endl;
	std::cout << GetTypeInfoPointer( &ca ).GetName() << std::endl;

	CObjB b( 5 );
	CObjA a( 7 );
	assert( DynamicCast<CObjB>( &a ) == 0 );
	printf( "%d\n", DYNAMIC_CAST<CObjB>( &b )->GetValue() );
	printf( "%d\n", DYNAMIC_CAST<CObjA>( &a )->GetValue() );
	printf( "%d\n", DYNAMIC_CAST<CObjA>( &b )->GetValue() );

	assert( DYNAMIC_CAST<CObjB>( &b )->GetValue() == 5 );
	assert( DYNAMIC_CAST<CObjA>( &a )->GetValue() == 7 );
	assert( DYNAMIC_CAST<CObjA>( &b )->GetValue() == 5 );

	assert( DYNAMIC_CAST<CObjD>( &a ) == 0 );
	assert( DYNAMIC_CAST<CObjC>( &a ) == 0 );
	CObjA* bPtrA = DYNAMIC_CAST<CObjA>( &b );
	CObjC c( 3 );
	printf( "%d\n", DYNAMIC_CAST<CObjB>( &c )->GetValue() );
	printf( "%d\n", DYNAMIC_CAST<CObjA>( &c )->GetValue() );
	CObjA* tmp = DYNAMIC_CAST<CObjA>( &c );
	printf( "%d\n", tmp->GetValue() );
	printf( "%d\n", DYNAMIC_CAST<CObjC>( tmp )->GetValue() );

	assert( DYNAMIC_CAST<CObjB>( &c )->GetValue() == 3 );
	assert( DYNAMIC_CAST<CObjA>( &c )->GetValue() == 3 );
	assert( tmp->GetValue() == 3 );
	assert( DYNAMIC_CAST<CObjC>( tmp )->GetValue() == 3 );


	CObjD d( 2 );
	assert( DYNAMIC_CAST<CObjC>( &d ) == 0 );

	assert( TYPEID( CA ) != TYPEID( CD ) );
	assert( TYPEID( CA ) == TYPEID( CA ) );
	assert( TYPEID( CA ) != TYPEID( CObjA ) );
	assert( TYPEID( CObjC ) == TYPEID( CObjC ) );

	// ��. ������������
	CObjE e( 11 );
	printf( "%d\n", DYNAMIC_CAST<CObjA>( &e )->GetValue() );
	printf( "%d\n", DYNAMIC_CAST<CObjA>( DYNAMIC_CAST<CObjF>( &e ) )->GetValue() );
	assert( DYNAMIC_CAST<CObjA>( &e )->GetValue() == 11 );
	assert( DYNAMIC_CAST<CObjA>( DYNAMIC_CAST<CObjF>( &e ) )->GetValue() == 11 );
	assert( DYNAMIC_CAST<CObjF>( &e ) != 0 );

	return 0;
}

