#pragma once

#include <map>
#include <vector>
#include <string>
#include <exception>

// ���������� ������������� ���������� � ����
class CRegisteredTypeInfo {
public:
	CRegisteredTypeInfo( const char* name, const char* definitionFile, int definitionLine, int id );

	std::string GetName() const;
	int GetHashCode() const;
	bool Equals( const CRegisteredTypeInfo* other ) const;
	bool CanBeUpcastedTo( const CRegisteredTypeInfo* upClass ) const;
	ptrdiff_t UpcastTo( const CRegisteredTypeInfo* upClass, ptrdiff_t object ) const;
	void AddParent( const CRegisteredTypeInfo* parent, ptrdiff_t shift );

private:
	std::string name;
	std::vector<const CRegisteredTypeInfo*> parents;
	std::vector<ptrdiff_t> shifts;
	const char* definitionFile;
	const int definitionLine;
	const int id;
};

//---------------------------------------------------------------------------------------------------------------------

// ��� ����������� �������������
int __GetTypeIdOrRegister( const char* name, const char* filename, int line );
CRegisteredTypeInfo* __GetTypeById( int id );
CRegisteredTypeInfo* __GetTypeOrRegister( const char* name, const char* filename, int line );
void __AddDerived( const CRegisteredTypeInfo* child, const CRegisteredTypeInfo* parent, ptrdiff_t difference );

// ���������� ��������� ��������� CTypeInfo �� ������ ��� �������� (����� ���� ����������� �������� ��� �� �������� - 
// ����� ����������� ����� �� CObject).
#define ENABLE_RTTI(T) class T; \
	template<>\
	class CTypeInfoGenerator<T> : public CTypeInfo { \
		public: \
		CTypeInfoGenerator() : CTypeInfo( __GetTypeOrRegister(#T, __FILE__, __LINE__ ) ) { }\
	};

//---------------------------------------------------------------------------------------------------------------------

// ���������� � ����
class CTypeInfo {
public:
	CTypeInfo( const CRegisteredTypeInfo* info );
	CTypeInfo( const CTypeInfo& other );

	std::string GetName() const; // ���
	int GetHashCode() const; // ���-���
	bool Equals( const CTypeInfo& other ) const;
	bool operator == ( const CTypeInfo& other );
	bool operator != ( const CTypeInfo& other );
private:
	const CRegisteredTypeInfo* info;
	// ��� ����������� �������������
	friend const CRegisteredTypeInfo* __GetInternalTypeInfo( const CTypeInfo& value );
};

//---------------------------------------------------------------------------------------------------------------------

template<typename T>
class CTypeInfoGenerator : public CTypeInfo {
public:
	CTypeInfoGenerator() : CTypeInfo( 0 )
	{
		throw std::logic_error( "No RTTI for this class" );
	}
	CTypeInfoGenerator( T& ) : CTypeInfoGenerator()
	{
	}
	CTypeInfoGenerator( T* ) : CTypeInfoGenerator()
	{
	}
};

template<typename T>
CTypeInfo GetTypeInfoValue( const T& value )
{
	return CTypeInfoGenerator<T>();
}

template<typename T>
CTypeInfo GetTypeInfoPointer( const T* value )
{
	return CTypeInfoGenerator<T>();
}

#define TYPEID(T) CTypeInfoGenerator<T>()

//---------------------------------------------------------------------------------------------------------------------

// ����� ��� ����������� �������������. ������������ ���������� ���������� � ������������
class __CDeriver {
public:
	__CDeriver( const CTypeInfo& children, const CTypeInfo& parent, ptrdiff_t shift )
	{
		__AddDerived( __GetInternalTypeInfo( children ), __GetInternalTypeInfo( parent ), shift );
	}
};

const ptrdiff_t __DeriverBase = 10000;

// ��� ����������� �������������: ���������� �������� ��� ����� �� ������ �������
template<typename Child, typename Parent>
ptrdiff_t __DiffClassPointers()
{
	return reinterpret_cast<ptrdiff_t>(static_cast<Parent*>(reinterpret_cast<Child*>(__DeriverBase))) - __DeriverBase;
}

#define DERIVED(Child, Parent) __CDeriver __Deriver##Child##Parent ( TYPEID(Child), TYPEID(Parent), __DiffClassPointers<Child, Parent>() );
