#include "Rtti.h"

CObjectWithRTTI::CObjectWithRTTI( const CRegisteredTypeInfo* _type ) : type( _type )
{
}

CTypeInfo CObjectWithRTTI::GetType() const
{
	return CTypeInfo( type );
}

const CRegisteredTypeInfo* CObjectWithRTTI::GetInternalTypeInfo() const
{
	return type;
}
