﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commands
{
    public class AllCommands
    {
        public static List<Command> GetAllCommands()
        {
            if (Commands == null)
            {
                Commands = new List<Command>();
                Commands.Add(new NopCommand());
                Commands.Add(new PopPrintCommand());
                Commands.Add(new TerminateCommand());
                Commands.Add(new PrintStrCommand());
                Commands.Add(new PrintIntCommand());
                Commands.Add(new ReadPushCommand());
                Commands.Add(new PushCommand());
                Commands.Add(new CallCommand());
                Commands.Add(new RetCommand());
                Commands.Add(new AddSSSCommand());
                Commands.Add(new SubSASCommand());
                Commands.Add(new CJumpESACommand());
                Commands.Add(new JumpCommand());
                Commands.Add(new MoveSSCommand());
                Commands.Add(new DebugCommand());
            }
            return Commands;
        }

        public static Command CommandById(byte id)
        {
            return GetAllCommands()[id];
        }

        public static int CommandIdByName(string name)
        {
            return GetAllCommands().FindIndex(x => x.Name().Equals(name));
        }

        public static int CommandsCount()
        {
            return GetAllCommands().Count;
        }

        public static void SetMachine(IMachine machine)
        {
            List<Command> commands = GetAllCommands();
            foreach (var item in commands)
            {
                item.Machine = machine;
            }

        }

        private static List<Command> Commands;
    }

    class NopCommand : Command
    {
        public override int ArgumentCount()
        {
            return 0;
        }

        public override string Decription()
        {
            return "Ничего не делает";
        }

        public override string Name()
        {
            return "nop";
        }

        protected override bool Action(byte first, byte second, byte third)
        {
            return true;
        }
    }

    class TerminateCommand : Command
    {
        public override int ArgumentCount()
        {
            return 0;
        }

        public override string Decription()
        {
            return "Останавливает работу машины";
        }

        public override string Name()
        {
            return "term";
        }

        protected override bool Action(byte first, byte second, byte third)
        {
            Machine.Terminate();
            return true;
        }
    }

    class PopPrintCommand : StackAndRegistersCommand
    {
        public override int ArgumentCount()
        {
            return 0;
        }

        public override string Decription()
        {
            return "Выводит на консоль число, лежащее на вершине стэка";
        }

        public override string Name()
        {
            return "popprint";
        }

        protected override bool Action(byte first, byte second, byte third)
        {
            Console.WriteLine(Pop());
            return true;
        }
    }

    class PrintStrCommand : Command
    {
        public override int ArgumentCount()
        {
            return 1;
        }

        public override string Decription()
        {
            return "Выводит на консоль строку, переданную в аргумент по адресу";
        }

        public override string Name()
        {
            return "printstr";
        }

        protected override bool Action(byte first, byte second, byte third)
        {
            byte index = first;
            bool inString = true;
            List<Byte> buffer = new List<byte>();
            while (inString)
            {
                buffer.AddRange(BitConverter.GetBytes(Machine.GetAt(index)));
                int zeroIndex = -1;
                for (int i = 0; i < sizeof(int); i++)
                {
                    int current = buffer.Count + i - sizeof(int);
                    if (buffer[current] == 0)
                    {
                        zeroIndex = current;
                        break;
                    }
                }
                if (zeroIndex != -1)
                {
                    inString = false;
                    Console.WriteLine(Encoding.UTF8.GetString(buffer.Take(zeroIndex).ToArray()));
                }
                else
                {
                    ++index;
                    if (index == 255)
                    {
                        new MachineException("Строка не завершена");
                    }
                }
            }
            return true;
        }
    }

    class PrintIntCommand : Command
    {
        public override int ArgumentCount()
        {
            return 1;
        }

        public override string Decription()
        {
            return "Выводит на консоль число, переданное по адресу в аргумент";
        }

        public override string Name()
        {
            return "printint";
        }

        protected override bool Action(byte first, byte second, byte third)
        {
            Console.WriteLine(Machine.GetAt(first));
            return true;
        }
    }

    class ReadPushCommand : StackAndRegistersCommand
    {
        public override int ArgumentCount()
        {
            return 0;
        }

        public override string Decription()
        {
            return "Считывает число с консоли и добавляет его на стэк";
        }

        public override string Name()
        {
            return "readpush";
        }

        protected override bool Action(byte first, byte second, byte third)
        {
            int value;
            if (!int.TryParse(Console.ReadLine(), out value))
            {
                throw new MachineException("Ввыдено не число");
            }
            Push(value);
            return true;
        }
    }

    class PushCommand : StackAndRegistersCommand
    {
        public override int ArgumentCount()
        {
            return 1;
        }

        public override string Decription()
        {
            return "Кладет на стэк число по адресу из 1 аргумента";
        }

        public override string Name()
        {
            return "push";
        }

        protected override bool Action(byte first, byte second, byte third)
        {
            Push(Machine.GetAt(first));
            return true;
        }
    }

    class CallCommand : StackAndRegistersCommand
    {
        public override int ArgumentCount()
        {
            return 3;
        }

        public override string Decription()
        {
            return "Вызов функции. В аргументы: вызываемая функция, число аргументов функции (должны лежать на вершине стэка), число возвращаемых аргументов.";
        }

        public override string Name()
        {
            return "call";
        }

        protected override bool Action(byte first, byte second, byte third)
        {
            CommandEntry entry = new CommandEntry();
            entry.CommandCode = InstuctionPointer;
            entry.Argument1 = second; // Кол-во аргументов
            entry.Argument2 = third; // Число возвращаемых значений
            entry.Argument3 = FramePointer;
            Push(entry.ToInt());
            InstuctionPointer = first; // Куда прыгаем
            FramePointer = StackPointer;
            return false;
        }
    }

    class RetCommand : StackAndRegistersCommand
    {
        public override int ArgumentCount()
        {
            return 2;
        }

        public override string Decription()
        {
            return "Возвращает управление из подпрограммы, очищает стэк. Два аргумента. Первый - смещение отностельно вершины стека возвращемых значений, второй - их количество.";
        }

        public override string Name()
        {
            return "ret";
        }

        protected override bool Action(byte first, byte second, byte third)
        {
            CommandEntry frameInfo = CommandEntry.FromInt(Machine.GetAt(FramePointer));
            if (frameInfo.Argument2 != second)
            {
                throw new MachineException("Возвращется не то число аргументов, что предполаголось");
            }
            int newStackPointer = FramePointer + frameInfo.Argument1 + 1 - frameInfo.Argument2;
            for (int i = 0; i < second; i++)
            {
                Set(Get(StackPointer + first + i), newStackPointer + i);
            }
            StackPointer = CheckInRange(newStackPointer, " при сдвиге вершины стэка");
            FramePointer = frameInfo.Argument3;
            InstuctionPointer = frameInfo.CommandCode;
            return true;
        }
    }

    class AddSSSCommand : StackAndRegistersCommand
    {
        public override int ArgumentCount()
        {
            return 3;
        }

        public override string Decription()
        {
            return "Сложение чисел. В аргументы - смещения отсносительно вершины стэка слагаемых и суммы";
        }

        public override string Name()
        {
            return "addsss";
        }

        protected override bool Action(byte first, byte second, byte third)
        {
            int a = GetOffset(first);
            int b = GetOffset(second);
            SetOffset(a + b, third);
            return true;
        }
    }

    class SubSASCommand : StackAndRegistersCommand
    {
        public override int ArgumentCount()
        {
            return 3;
        }

        public override string Decription()
        {
            return "Вычитание чисел. В аргументы - смещения отсносительно вершины стэка вычитаемого (1 арг) и разности (3 арг). Второй аргумент - адрес вычитаемого";
        }

        public override string Name()
        {
            return "subsas";
        }

        protected override bool Action(byte first, byte second, byte third)
        {
            int a = GetOffset(first);
            int b = Machine.GetAt(second);
            SetOffset(a - b, third);
            return true;
        }
    }

    class CJumpESACommand : StackAndRegistersCommand
    {
        public override int ArgumentCount()
        {
            return 3;
        }

        public override string Decription()
        {
            return "Условный прыжок в случае равенства чисел. Сначала адрес перехода. Потом - смещение от вершины стэка первого числа. Затем - адрес второго числа";
        }

        public override string Name()
        {
            return "cjumpesa";
        }

        protected override bool Action(byte first, byte second, byte third)
        {
            int a = GetOffset(second);
            int b = Machine.GetAt(third);
            if (a == b)
            {
                InstuctionPointer = first;
                return false;
            }
            return true;
        }
    }

    class JumpCommand : StackAndRegistersCommand
    {
        public override int ArgumentCount()
        {
            return 1;
        }

        public override string Decription()
        {
            return "Безусловный прыжок по адресу из единственного аргумента";
        }

        public override string Name()
        {
            return "jump";
        }

        protected override bool Action(byte first, byte second, byte third)
        {
            InstuctionPointer = first;
            return false;
        }
    }

    class MoveSSCommand : StackAndRegistersCommand
    {
        public override int ArgumentCount()
        {
            return 2;
        }

        public override string Decription()
        {
            return "Присваевает значение со смещением от вершины стэка в первом аргументе в память со смещением во втором аргументе";
        }

        public override string Name()
        {
            return "movess";
        }

        protected override bool Action(byte first, byte second, byte third)
        {
            SetOffset(GetOffset(first), second);
            return true;
        }
    }

    class DebugCommand : StackAndRegistersCommand
    {
        public override int ArgumentCount()
        {
            return 0;
        }

        public override string Decription()
        {
            return "Отладочный вывод важных значений памяти";
        }

        public override string Name()
        {
            return "debug";
        }

        protected override bool Action(byte first, byte second, byte third)
        {
            CommandEntry entry = CommandEntry.FromInt(Machine.GetAt(InstuctionPointer));
            string commandName = "error";
            if (entry.CommandCode < AllCommands.CommandsCount())
            {
                commandName = AllCommands.CommandById(entry.CommandCode).Name();
            }
            Console.Error.WriteLine(">IP {0,-3}; SP {1,-3}; FP {2,-3}. Next: {4,-3}: {5,-3}, {6,-3}, {7,-3} ({3})",
                InstuctionPointer, StackPointer, FramePointer, commandName,
                entry.CommandCode, entry.Argument1, entry.Argument2, entry.Argument3);
#if PRINT_STACK
            for (int i = StackPointer; i < 256; i++)
            {
                Console.Error.WriteLine("> - Stack: {0} {1}", i, Get(i));
            }
#endif
            return true;
        }
    }
}
