﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commands
{
    public interface IMachine
    {
        int GetAt(byte index);
        void SetAt(int value, byte index);
        void Terminate();
    }
}
