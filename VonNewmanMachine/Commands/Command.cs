﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Commands
{
    public abstract class Command
    {
        public IMachine Machine { get; set; }
        public bool Invoke(byte first, byte second, byte third)
        {
            if (Machine == null)
            {
                throw new NullReferenceException();
            }
            int argCount = ArgumentCount();
            switch (argCount)
            {
                case -1:
                case 3:
                    break;
                case 0:
                    Check0Arguments(first, second, third);
                    break;
                case 1:
                    Check1Arguments(first, second, third);
                    break;
                case 2:
                    Check2Arguments(first, second, third);
                    break;
                default:
                    Debug.Assert(false);
                    break;
            }

            return Action(first, second, third);
        }

        public abstract String Name();
        public abstract int ArgumentCount();
        public abstract String Decription();

        protected abstract bool Action(byte first, byte second, byte third);

        protected void Check0Arguments(byte first, byte second, byte third)
        {
            if (first != 0 || second != 0 || third != 0)
            {
                throw new MachineException("без аргументов", Name());
            }
        }
        protected void Check1Arguments(byte first, byte second, byte third)
        {
            if (second != 0 || third != 0)
            {
                throw new MachineException("принимает один аргумент", Name());
            }
        }
        protected void Check2Arguments(byte first, byte second, byte third)
        {
            if (third != 0)
            {
                throw new MachineException("принимает два аргумента", Name());
            }
        }
    }

    public class MachineException : Exception
    {
        public MachineException()
        {
            Init();
        }

        public MachineException(String message) : base(message)
        {
            Init();
        }

        public MachineException(String message, String name) : base(String.Format("Инструкция {0} {1}", name, message))
        {
            Init();
        }

        public int Address { get; set; }

        public override string Message
        {
            get
            {
                if (Address >= 0)
                {
                    return String.Format("Ошибка инструкции по адресу {0}: {1}", Address, base.Message);
                }
                else
                {
                    return "Ошибка машины: " + base.Message;
                }
            }
        }

        private void Init()
        {
            Address = -1;
        }

    }

    public class CommandEntry
    {
        public byte CommandCode;
        public byte Argument1;
        public byte Argument2;
        public byte Argument3;

        public int ToInt()
        {
            int result = CommandCode;
            result <<= 8;
            result += Argument1;
            result <<= 8;
            result += Argument2;
            result <<= 8;
            result += Argument3;
            return result;
        }

        public bool Invoke()
        {
            var commnads = AllCommands.GetAllCommands();
            if (commnads.Count >= CommandCode)
            {
                throw new MachineException(String.Format("Недопустимая инструкция с кодом {0}.", CommandCode));
            }
            return commnads[CommandCode].Invoke(Argument1, Argument2, Argument3);
        }

        public static CommandEntry FromInt(int value)
        {
            CommandEntry result = new CommandEntry();
            result.Argument3 = (byte)(value % 256);
            value >>= 8;
            result.Argument2 = (byte)(value % 256);
            value >>= 8;
            result.Argument1 = (byte)(value % 256);
            value >>= 8;
            result.CommandCode = (byte)(value % 256);
            return result;
        }
    }
}
