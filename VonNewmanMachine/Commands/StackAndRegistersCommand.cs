﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commands
{
    abstract class StackAndRegistersCommand : Command
    {
        protected byte InstuctionPointer
        {
            get { return GetInstuctionPointer(); }
            set { SetInstructionPointer(value); }
        }

        protected byte StackPointer
        {
            get { return GetStackPointer(); }
            set { SetStackPointer(value); }
        }

        protected byte FramePointer
        {
            get { return GetFramePointer(); }
            set { SetFramePointer(value); }
        }

        protected byte GetInstuctionPointer()
        {
            return GetRegisterData()[0];
        }

        protected byte GetStackPointer()
        {
            return GetRegisterData()[1];
        }

        protected byte GetFramePointer()
        {
            return GetRegisterData()[2];
        }

        protected void SetInstructionPointer(byte value)
        {
            byte[] data = GetRegisterData();
            data[0] = value;
            SetRegisterData(data);
        }

        protected void SetStackPointer(byte value)
        {
            byte[] data = GetRegisterData();
            data[1] = value;
            SetRegisterData(data);
        }

        protected void SetFramePointer(byte value)
        {
            byte[] data = GetRegisterData();
            data[2] = value;
            SetRegisterData(data);
        }

        protected void Push(int value)
        {
            if (StackPointer == 0)
            {
                throw new MachineException("Переполнение стека");
            }
            StackPointer--;
            Machine.SetAt(value, StackPointer);
        }

        protected int Pop()
        {
            if (StackPointer == 255)
            {
                throw new MachineException("Попытка взять даныне с пустого стека");
            }
            return Machine.GetAt(StackPointer++);
        }

        protected int Get(int address)
        {
            CheckInRange(address, "на чтение");
            return Machine.GetAt((byte)address);
        }

        protected void Set(int value, int address)
        {
            CheckInRange(address, "на запись");
            Machine.SetAt(value, (byte)address);
        }

        protected int GetOffset(int offset)
        {
            return Get(StackPointer + offset);
        }

        protected void SetOffset(int value, int offset)
        {
            Set(value, StackPointer + offset);
        }

        protected static byte CheckInRange(int address, string info)
        {
            if (address < 0 || address > 255)
            {
                throw new MachineException(String.Format("Обращение за границы памяти {0}", info));
            }
            return (byte)address;
        }

        private byte[] GetRegisterData()
        {
            return BitConverter.GetBytes(Machine.GetAt(0));
        }

        private void SetRegisterData(byte[] data)
        {
            Machine.SetAt(BitConverter.ToInt32(data, 0), 0);
        }
    }
}
