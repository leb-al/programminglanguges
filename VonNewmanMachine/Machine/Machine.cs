﻿using Commands;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machine
{
    public class Machine : IMachine
    {
        private const int MemorySize = 256;
        private int[] Memory;
        bool Active = false;

        public Machine()
        {
            Memory = new int[MemorySize];
            CommandEntry entry = new CommandEntry();
            entry.CommandCode = (byte)AllCommands.CommandIdByName("term");
            Memory[0] = BitConverter.ToInt32(new byte[] { 1, 255, 255, 0 }, 0);
            Memory[1] = entry.ToInt();
        }

        public void Load(String filename)
        {
            byte[] bytes = File.ReadAllBytes(filename);
            if (bytes.Length != MemorySize * sizeof(int))
            {
                throw new MachineException("Неправильный размер памяти в образе");
            }
            for (int i = 0; i < MemorySize; i++)
            {
                Memory[i] = BitConverter.ToInt32(bytes, i * sizeof(int));
            }
        }

        public void Run()
        {
            if (Memory.Length != MemorySize)
            {
                throw new MachineException("Неправильный размер памяти");
            }

            Active = true;
            AllCommands.SetMachine(this);

            while (Active)
            {
#if DEBUG
                AllCommands.CommandById((byte)AllCommands.CommandIdByName("debug")).Invoke(0, 0, 0);
#endif
                int instructionPointer = BitConverter.GetBytes(Memory[0])[0];
                CommandEntry entry = CommandEntry.FromInt(Memory[instructionPointer]);
                if (entry.CommandCode >= AllCommands.CommandsCount())
                {
                    throw new MachineException(String.Format("Неверная инструкция {0} по адресу {1}", entry.CommandCode, instructionPointer));
                }
                if (AllCommands.CommandById(entry.CommandCode).Invoke(entry.Argument1, entry.Argument2, entry.Argument3) && Active)
                {
                    byte[] registers = BitConverter.GetBytes(Memory[0]);
                    if (registers[0] == 255)
                    {
                        throw new MachineException("Указатель на последнюю инструкцию в памяти сдвигается вперед");
                    }
                    ++registers[0];
                    Memory[0] = BitConverter.ToInt32(registers, 0);
                }
            }

        }

        public int GetAt(byte index)
        {
            return Memory[index];
        }

        public void SetAt(int value, byte index)
        {
            Memory[index] = value;
        }

        public void Terminate()
        {
            Active = false;
        }
    }
}
