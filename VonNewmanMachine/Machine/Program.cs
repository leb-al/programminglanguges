﻿using Commands;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machine
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (string filename in args)
            {
                try
                {
                    Console.WriteLine(">Loading {0}", filename);
                    Machine machine = new Machine();
                    machine.Load(filename);
                    Console.WriteLine(">Launching");
                    machine.Run();
                }
                catch (IOException e)
                {
                    Console.Error.WriteLine(">Ошибка ввода/вывода: {0}", e.Message);
                }
                catch (MachineException e)
                {
                    Console.Error.WriteLine(">{0}", e.Message);
                }
            }
        }
    }
}
