﻿using Commands;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assembler
{
    class Program
    {
        private const string IoExceptionMessage = "Ошибка ввода/вывода: {0}";
        private const string AssemblerParam = "a";
        private const string DisassemblerParam = "d";

        static void Main(string[] args)
        {
            if (args.Length != 3 || (!(args[0].Equals(AssemblerParam) || args[0].Equals(DisassemblerParam))))
            {
                PrintHelp();
            }
            else
            {
                if (args[0].Equals(AssemblerParam))
                {
                    try
                    {
                        Application app = new Application();
                        app.CreateFromFile(args[1]);
                        app.WriteFile(args[2]);
                    }
                    catch (IOException e)
                    {
                        Console.Error.WriteLine(IoExceptionMessage, e.Message);
                    }
                    catch (AssemblerException e)
                    {
                        Console.Error.WriteLine(e.Message);
                    }
                }
                else
                {
                    try
                    {
                        Application app = new Application();
                        app.CreateFromDump(args[1]);
                        app.WriteAssemblerFile(args[2]);
                    }
                    catch (IOException e)
                    {
                        Console.Error.WriteLine(IoExceptionMessage, e.Message);
                    }
                    catch (AssemblerException e)
                    {
                        Console.Error.WriteLine(e.Message);
                    }
                }
            }
        }

        private static void PrintHelp()
        {
            Console.Error.WriteLine("Использование: assembler <param> <source> <destination>");
            Console.Error.WriteLine("<param> = {0} для ассемблирования", AssemblerParam);
            Console.Error.WriteLine("<param> = {0} для дизассемблирования", DisassemblerParam);
            Console.Error.WriteLine("<source> <destination> - имена файлов");
            Console.Error.WriteLine();

            Console.Error.WriteLine("Описание синтаксиса ассемблера");
            Console.Error.WriteLine();

            Console.Error.WriteLine("Сначала идет секция данных");
            Console.Error.WriteLine("Формат строки: <тип данных> <имя метки> <данные>");
            Console.Error.WriteLine("Типы данных");
            Console.Error.WriteLine("{0} - все, что после имени метки и пробельного символа до конца строки - записывается строкой в данные", Application.DataStringCommand);
            Console.Error.WriteLine("{0} - после метки записывается один int, он и добавляется данные", Application.DataIntCommand);
            Console.Error.WriteLine("{0} - после имени метки идет произвольное (один и больше) число int-ов, они и записываются в данные", Application.DataArrayCommand);
            Console.Error.WriteLine("{0} - окончание секции данных. Далее идет секция кода. Нет аргументов", Application.DataEndCommand);
            Console.Error.WriteLine();

            Console.Error.WriteLine("Секция кода");
            Console.Error.WriteLine("Формат:");
            Console.Error.WriteLine("label <labelname>  - обявление метки. Имя метки - произвольная последовательность непробельных символов, не являющаяся записью числа типа byte");
            Console.Error.WriteLine("<имя команды> [<аргумент 1> [<аргумент 2> [<аргумент 3>]]] -вызов команды ([] - опциональность)");
            Console.Error.WriteLine("Если аргумент является числом byte - то он сразу передается в аргумекнт, иначе воспринимается как имя метки и подставляется её адрес");
            Console.Error.WriteLine();

            Console.Error.WriteLine("Описания команд");
            Console.Error.WriteLine(" Id |арг| Имя        | Описание");
            var commands = AllCommands.GetAllCommands();
            for (int i = 0; i < commands.Count; i++)
            {
                Console.Error.WriteLine("{0:000} | {1:0} | {2,-10} | {3}", i, commands[i].ArgumentCount(), commands[i].Name(), commands[i].Decription());
            }
        }
    }
}