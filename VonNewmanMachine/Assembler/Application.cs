﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commands;
using System.IO;
using System.Diagnostics;

namespace Assembler
{
    public class Application
    {
        public const string DataEndCommand = "dataend";
        public const string DataArrayCommand = "dataarr";
        public const string DataIntCommand = "dataint";
        public const string DataStringCommand = "datastr";
        private const int MemorySize = 256;
        private List<CommandEntry> CommandEntries;
        private List<DataEntry> Data;
        private int[] Image;

        public void WriteFile(string filename)
        {
            if (CommandEntries == null)
            {
                throw new AssemblerException("Программа не загружена");
            }

            Debug.Assert(Image.Length == MemorySize);
            FileStream stream = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read);

            for (int i = 0; i < Image.Length; i++)
            {
                stream.Write(BitConverter.GetBytes(Image[i]), 0, 4);
            }

            stream.Flush();
            stream.Close();
        }

        public void CreateFromFile(string filename)
        {
            string[] lines = File.ReadAllLines(filename);

            List<CommandEntry> commandEntries = new List<CommandEntry>();
            List<DataEntry> data = new List<DataEntry>();
            int[] image = new int[MemorySize];

            SortedDictionary<String, int> labels = new SortedDictionary<string, int>();
            List<LabelReference> references = new List<LabelReference>();
            int imageIndex = 1;

            int lineIndex = CollectResources(lines, data, image, labels, ref imageIndex);

            int codeImageIndex = imageIndex;
            CollectCommands(lines, commandEntries, labels, references, ref imageIndex, ref lineIndex);

            ReplaceReferncesToValues(labels, references, lineIndex);

            FillMemoryByCommands(commandEntries, image, codeImageIndex);

            Image = image;
            Data = data;
            CommandEntries = commandEntries;
        }

        public void CreateFromDump(string filename)
        {
            byte[] bytes = File.ReadAllBytes(filename);
            if (bytes.Length != MemorySize * sizeof(int))
            {
                throw new AssemblerException("Неправильный размер памяти в образе");
            }

            List<CommandEntry> commandEntries = new List<CommandEntry>();
            List<DataEntry> data = new List<DataEntry>();
            int[] image = new int[MemorySize];

            for (int i = 0; i < MemorySize; i++)
            {
                image[i] = BitConverter.ToInt32(bytes, i * sizeof(int));
            }

            var codeAddress = bytes[0];
            for (int i = codeAddress; i < MemorySize; i++)
            {
                commandEntries.Add(CommandEntry.FromInt(image[i]));
            }

            FilterAndValidateCommands(commandEntries);

            if (codeAddress > 1)
            {
                DataEntry entry = new DataEntry() { Address = 1, Name = "alldata" };
                entry.Data = image.Skip(1).Take(codeAddress - 1).ToArray();
                data.Add(entry);
            }

            Image = image;
            Data = data;
            CommandEntries = commandEntries;
        }

        public void WriteAssemblerFile(string filename)
        {
            using (FileStream stream = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read))
            {
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    WriteData(writer);

                    writer.WriteLine(DataEndCommand);

                    WriteCommands(writer);

                    writer.Flush();
                    writer.Close();
                }
            }
        }

        private void WriteCommands(StreamWriter writer)
        {
            for (int i = 0; i < CommandEntries.Count; i++)
            {
                CommandEntry entry = CommandEntries[i];
                Debug.Assert(entry.CommandCode < AllCommands.CommandsCount());
                Command command = AllCommands.CommandById(entry.CommandCode);
                writer.Write(command.Name());
                for (int j = 0; j < command.ArgumentCount(); j++)
                {
                    switch (j)
                    {
                        case 0:
                            writer.Write(" {0}", entry.Argument1);
                            break;
                        case 1:
                            writer.Write(" {0}", entry.Argument2);
                            break;
                        case 2:
                            writer.Write(" {0}", entry.Argument3);
                            break;
                        default:
                            Debug.Assert(false);
                            break;
                    }
                }
                writer.WriteLine();
            }
        }

        private void WriteData(StreamWriter writer)
        {
            for (int i = 0; i < Data.Count; i++)
            {
                writer.Write(Data[i].Data.Length == 1 ? DataIntCommand : DataArrayCommand);
                writer.Write(" d{0}", i);
                for (int j = 0; j < Data[i].Data.Length; j++)
                {
                    writer.Write(" {0}", Data[i].Data[j]);
                }
                writer.WriteLine();
            }
        }

        private static void FilterAndValidateCommands(List<CommandEntry> commandEntries)
        {
            for (int i = commandEntries.Count - 1; i >= 0; i--)
            {
                if (commandEntries[i].CommandCode != 0)
                {
                    break;
                }
                commandEntries.RemoveAt(i);
            }

            for (int i = commandEntries.Count - 1; i >= 0; i--)
            {
                if (commandEntries[i].CommandCode >= AllCommands.CommandsCount())
                {
                    throw new AssemblerException("Неверная команда");
                }
            }
        }

        private static void FillMemoryByCommands(List<CommandEntry> commandEntries, int[] image, int codeImageIndex)
        {
            for (int i = 0; i < commandEntries.Count; i++)
            {
                image[codeImageIndex + i] = commandEntries[i].ToInt();
            }
            image[0] = BitConverter.ToInt32(new byte[] { (byte)codeImageIndex, 255, 255, 0 }, 0);
        }

        private static void ReplaceReferncesToValues(SortedDictionary<string, int> labels, List<LabelReference> references, int lineIndex)
        {
            foreach (LabelReference item in references)
            {
                if (!labels.ContainsKey(item.Name))
                {
                    throw new AssemblerException("Обращение к несуществующей метке", lineIndex);
                }
                int address = labels[item.Name];
                Debug.Assert(address >= 0 && address < MemorySize);
                switch (item.ArgumentNumber)
                {
                    case 1:
                        item.Entry.Argument1 = (byte)address;
                        break;
                    case 2:
                        item.Entry.Argument2 = (byte)address;
                        break;
                    case 3:
                        item.Entry.Argument3 = (byte)address;
                        break;
                    default:
                        Debug.Assert(false);
                        break;
                }
            }
        }

        private static void CollectCommands(string[] lines, List<CommandEntry> commandEntries, SortedDictionary<string, int> labels, List<LabelReference> references, ref int imageIndex, ref int lineIndex)
        {
            for (; lineIndex < lines.Length; lineIndex++)
            {
                string currentLine = lines[lineIndex].Trim();
                string[] tokens = currentLine.Split();
                if (tokens.Length == 0)
                {
                    throw new AssemblerException("Неверный синтаксис в секции кода", lineIndex);
                }
                if (tokens[0].Equals("label"))
                {
                    if (tokens.Length != 2)
                    {
                        throw new AssemblerException("Неверный синтаксис в объявлении метки", lineIndex);
                    }
                    CheckValidLabelName(tokens[1], lineIndex);
                    labels.Add(tokens[1], imageIndex);
                }
                else
                {
                    int commandId = AllCommands.CommandIdByName(tokens[0]);
                    Debug.Assert(commandId < MemorySize);
                    if (commandId < 0)
                    {
                        throw new AssemblerException("Неверная команда", lineIndex);
                    }

                    Command command = AllCommands.CommandById((byte)commandId);
                    if ((command.ArgumentCount() >= 0) && (command.ArgumentCount() != (tokens.Length - 1)))
                    {
                        throw new AssemblerException("Неверное число аргументов у команды", lineIndex);
                    }

                    CommandEntry entry = new CommandEntry();
                    entry.CommandCode = (byte)commandId;

                    CollectCommandArguments(references, lineIndex, tokens, entry);

                    commandEntries.Add(entry);
                    ++imageIndex;
                    if (imageIndex > 255)
                    {
                        throw new AssemblerException("Не хватает памяти для программы", lineIndex);
                    }
                }
            }
        }

        private static void CollectCommandArguments(List<LabelReference> references, int lineIndex, string[] tokens, CommandEntry entry)
        {
            for (int i = 1; i < tokens.Length; i++)
            {
                if (IsValidLabelName(tokens[i]))
                {
                    references.Add(new LabelReference() { Name = tokens[i], Entry = entry, ArgumentNumber = i, StringNumber = lineIndex });
                }
                else
                {
                    switch (i)
                    {
                        case 1:
                            entry.Argument1 = CheckByte(tokens[i], lineIndex);
                            break;
                        case 2:
                            entry.Argument2 = CheckByte(tokens[i], lineIndex);
                            break;
                        case 3:
                            entry.Argument3 = CheckByte(tokens[i], lineIndex);
                            break;
                        default:
                            Debug.Assert(false);
                            break;
                    }
                }
            }
        }

        private int CollectResources(string[] lines, List<DataEntry> data, int[] image, SortedDictionary<string, int> labels, ref int imageIndex)
        {
            int lineIndex = 0;
            bool resourceSection = true;
            while (resourceSection)
            {
                string currentLine = lines[lineIndex].Trim();
                string[] tokens = currentLine.Split();
                DataEntry entry = new DataEntry();

                if (tokens.Length == 1 && tokens[0].Equals(DataEndCommand))
                {
                    tokens = new string[] { tokens[0], "", "" };
                }
                if (tokens.Length < 3)
                {
                    throw new AssemblerException("Неверный синтаксис в секции данных (мало аргументов)", lineIndex);
                }
                entry.Name = tokens[1];
                CheckValidLabelName(entry.Name, lineIndex);
                entry.Address = imageIndex;


                switch (tokens[0])
                {
                    case DataStringCommand:
                        entry.Data = GetString(imageIndex, currentLine, tokens);
                        break;
                    case DataIntCommand:
                        entry.Data = new int[] { CheckInt(tokens[2], lineIndex) };
                        if (tokens.Length > 3)
                        {
                            throw new AssemblerException("Неверный синтаксис в секции данных (много аргументов)", lineIndex);
                        }
                        break;
                    case DataArrayCommand:
                        entry.Data = tokens.Skip(2).Select(x => CheckInt(x, lineIndex)).ToArray();
                        break;
                    case DataEndCommand:
                        resourceSection = false;
                        break;
                    default:
                        throw new AssemblerException("Неожиданная команда в секции данных", lineIndex);
                }

                if (resourceSection)
                {
                    if (entry.Data.Length + imageIndex > 255)
                    {
                        throw new AssemblerException("Выход за пределы памяти", lineIndex);
                    }
                    entry.Data.CopyTo(image, imageIndex);
                    imageIndex += entry.Data.Length;
                    data.Add(entry);
                    labels.Add(entry.Name, entry.Address);
                }

                if ((lineIndex++) >= lines.Length)
                {
                    throw new AssemblerException("Неожиданный конец файла", lineIndex);
                }
            }

            return lineIndex;
        }

        private static int[] GetString(int imageIndex, string currentLine, string[] tokens)
        {
            string value = currentLine.Substring(tokens[0].Length + tokens[1].Length + 2);
            byte[] stringData = Encoding.UTF8.GetBytes(value);
            int[] resultData = new int[stringData.Length / 4 + 1];
            stringData = stringData.Concat(new byte[resultData.Length * 4 - stringData.Length]).ToArray();
            for (int i = 0; i < resultData.Length; i++)
            {
                resultData[i] = BitConverter.ToInt32(stringData, 4 * i);
            }
            return resultData;
        }

        private static bool IsValidLabelName(string name)
        {
            byte tmp;
            return (!byte.TryParse(name, out tmp));
        }

        private static void CheckValidLabelName(string name, int line)
        {
            if (!IsValidLabelName(name))
            {
                throw new AssemblerException("Неправильное имя метки", line);
            }
        }

        private static int CheckInt(string value, int line)
        {
            int result;
            if (!int.TryParse(value, out result))
            {
                throw new AssemblerException("Неправильное число (int)", line);
            }
            return result;
        }

        private static byte CheckByte(string value, int line)
        {
            byte result;
            if (!byte.TryParse(value, out result))
            {
                throw new AssemblerException("Неправильное число (byte)", line);
            }
            return result;
        }
    }

    class DataEntry
    {
        internal int[] Data;
        internal int Address;
        internal String Name;
    }

    class LabelReference
    {
        internal string Name;
        internal CommandEntry Entry;
        internal int ArgumentNumber;
        internal int StringNumber;
    }

    class AssemblerException : Exception
    {
        public AssemblerException(string message, int line = -1) : base(message)
        {
            this.Line = line + 1;
        }

        public override string Message
        {
            get
            {
                if (Line > 0)
                {
                    return String.Format("Ошибка ассемблера (строка {0}): {1}", Line, base.Message);
                }
                else
                {
                    return String.Format("Ошибка ассемблера: {1}", Line, base.Message);

                }
            }
        }

        private int Line;
    }
}
