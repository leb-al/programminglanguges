// FuturePromise.cpp: ���������� ����� ����� ��� ����������� ����������.
//


#include <vector>
#include <thread>
//#include <future>
#include <numeric>
#include <iostream>
#include <chrono>

#include "future.h"
#include "Promise.h"
#include "Async.h"

void accumulate( std::vector<int>::iterator first,
	std::vector<int>::iterator last,
	CPromise<int>& accumulate_promise )
{
	int sum = std::accumulate( first, last, 0 );
	//accumulate_promise.SetException( new std::logic_error("Test") );
	accumulate_promise.SetValue( sum );  // Notify future
}

int main()
{
	// Demonstrate using promise<int> to transmit a result between threads.
	std::vector<int> numbers = { 1, 2, 3, 4, 5, 6 };
	CPromise<int> accumulate_promise;
	CFuture<int> accumulate_future = accumulate_promise.GetFuture();
	int res = -1;
	assert( !accumulate_future.TryGet( res ) && res == -1 && !accumulate_future.TryGet() );
	std::thread work_thread( accumulate, numbers.begin(), numbers.end(),
		std::move( accumulate_promise ) );
	std::cout << "result=" << accumulate_future.Get() << '\n';
	assert( accumulate_future.TryGet( res ) && res == 21 );
	work_thread.join();  // wait for thread completion

	CFuture<int> f2 = async<int>( ST_Thread, []() { return 8; } );
	CFuture<int> f5 = async<int>( ST_Poll, []() { return 7; } );
	std::cout << f2.Get() << std::endl;
	std::cout << f5.Get() << std::endl;
	CFuture<int> f6 = async<int>( ST_Thread, []() { return 6; } );
	CFuture<int> f7 = async<int>( ST_Poll, []() { return 7; } );
	std::cout << f6.Get() << std::endl;
	std::cout << f7.Get() << std::endl;
	
	return 0;
}

