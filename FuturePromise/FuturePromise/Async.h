#pragma once

#include <atomic>
#include "Promise.h"
#include "ThreadPoll.h"


static CThreadPool __AsyncThreadPool( std::thread::hardware_concurrency() );

enum TStartType {
	ST_Thread,
	ST_Poll,
	ST_Sync,
	ST_PollOrSync
};


template<typename T, typename D>
CFuture<T> async( TStartType type, D& method )
{
	std::shared_ptr<CPromiseTask<T>> task( new CPromiseTask<T>( method ) );
	CFuture<T> future = task->GetFuture();
	switch( type ) {
		case ST_Thread:
		{
			std::thread workThread( [task] {
				std::dynamic_pointer_cast<CBasePromiseTask>(task).get()->operator()();
			} );
			workThread.detach();
			break;
		}
		case ST_Poll:
		{
			__AsyncThreadPool.AddTask( std::dynamic_pointer_cast<CBasePromiseTask>(task) );
			break;
		}
		case ST_Sync:
		{
			std::dynamic_pointer_cast<CBasePromiseTask>(task).get()->operator()();
			break;
		}
		case ST_PollOrSync:
		{
			if( __AsyncThreadPool.Availiable() ) {
				__AsyncThreadPool.AddTask( std::dynamic_pointer_cast<CBasePromiseTask>(task) );
			} else {
				std::dynamic_pointer_cast<CBasePromiseTask>(task).get()->operator()();
			}
			break;
		}
		default:
			assert( false );
			break;
	}
	return future;
}



template<typename T, typename D>
CFuture<std::pair<T, D> > FutureThan( CFuture<T>&& first, CFuture<D>&& second )
{
	std::atomic<bool> notMoved( true );
	CFuture<std::pair<T, D> > result = async<std::pair<T, D>>( ST_Thread, []( CFuture<T>&& _first, CFuture<D>&& _second ) {
		CFuture<T> localfirst( std::move( _first ) );
		CFuture<D> localsecond( std::move( _second ) );
		T firstResult = localfirst.Get();
		try {
			D secondResult = localsecond.Get();
			return std::pair<T, D>( firstResult, secondResult );
		}
		catch( std::exception* e ) {
			delete e;
			D defaultResult;
			return std::pair<T, D>( firstResult, defaultResult );
		}
	},/* std::move(*/ first /*)*/, /*std::move(*/ second/* )*/ );
	while( notMoved ) {
		std::this_thread::sleep_for( std::chrono::microseconds( 1 ) );
	}
	return result;
}