#pragma once
#include <exception>
#include "Future.h"
template <typename T>
class CPromise {
public:
	CPromise() {}
	~CPromise() {}

	CPromise( CPromise&& other )
	{
		std::lock_guard<std::mutex> guard( __FutureDestroyMutex );
		owner.SwapValues( &other.owner );
	}

	CPromise& operator = ( CPromise&& other )
	{
		std::lock_guard<std::mutex> guard( __FutureDestroyMutex );
		owner.SwapValues( &other.owner );
		return *this;
	}


	void SetValue( T& value )
	{
		std::lock_guard<std::mutex> guard( __FutureDestroyMutex );
		if( owner.HasValue() ) {
			owner.GetValue()->setValue( value );
		} else {
			if( !owner.IsCreated() ) {
				throw std::logic_error( "Can't set value to promise with no future" );
			}
		}
	}

	void SetException( std::exception* e )
	{
		std::lock_guard<std::mutex> guard( __FutureDestroyMutex );
		if( owner.HasValue() ) {
			owner.GetValue()->setException( e );
		} else {
			if( !owner.IsCreated() ) {
				throw std::logic_error( "Can't set value to promise with no future" );
			}
		}
	}

	CFuture<T> GetFuture()
	{
		std::lock_guard<std::mutex> guard( __FutureDestroyMutex );
		if( owner.HasValue() || owner.IsCreated() ) {
			throw std::logic_error( "Can't create more than one future" );
		}
		return CFuture<T>( &owner );
	}

	CPromise( const CPromise& other ) = delete;
private:

	__CFutureOwner<T> owner;
};


