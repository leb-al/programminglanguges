#pragma once
#include <mutex>
#include <cassert>
#include <utility>

template <typename T>
class __CFutureOwner;

template <typename T>
class CPromise;

extern std::mutex __FutureDestroyMutex;

template <typename T>
class CFuture {
private:
	inline void swap( CFuture& other );

public:
	CFuture( CFuture&& other ) : owner( 0 ), isReady( false ), exception( 0 )
	{
		swap( other );
	}

	~CFuture()
	{
		std::lock_guard<std::mutex> guard( __FutureDestroyMutex );
		if( owner != 0 ) {
			owner->Clear();
		}
	}

	CFuture operator = ( CFuture&& other )
	{
		swap( other );
		return *this;
	}

	// ��������� ����� � ������� ����������� ����������
	T Get() const
	{
		while( !TryGet() ) {
			std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
		}
		return value;
	}

	// ������������� ������� ���������. ���� ��������� � �������, ����������� ��� � result
	bool TryGet( T& result ) const
	{
		bool finish = TryGet();
		if( finish ) {
			result = value;
		}
		return finish;
	}

	// ������������� ������� ���������. ���������� ������� ����������
	bool TryGet() const
	{
		std::lock_guard<std::mutex> guard( mutex );
		if( exception != 0 ) {
			throw exception;
		}
		return isReady;
	}

private:
	CFuture( const CFuture& other ) = delete;

	CFuture( __CFutureOwner<T>* owner ) : owner( owner ), isReady( false ), exception( 0 )
	{
		owner->created = true;
		owner->value = this;
	}

	void setValue( const T& result )
	{
		std::lock_guard<std::mutex> guard( mutex );
		assert( !isReady );
		value = result;
		isReady = true;
	}

	void setException( std::exception* e )
	{
		std::lock_guard<std::mutex> guard( mutex );
		assert( !isReady );
		assert( exception == 0 );
		exception = e;
	}

	__CFutureOwner<T>* owner;
	mutable std::mutex mutex;
	T value;
	bool isReady;
	std::exception* exception;
	friend class CPromise<T>;
	friend class __CFutureOwner<T>;


};

template<typename T>
void CFuture<T>::swap( CFuture& b )
{
	std::lock_guard<std::mutex> guard( __FutureDestroyMutex );
	std::lock_guard<std::mutex> guard2( mutex );
	std::lock_guard<std::mutex> guard3( b.mutex );

	std::swap( this->owner, b.owner );
	if( owner != 0 ) {
		owner->SwapValues( b.owner, this );
	} else {
		if( b.owner != 0 ) {
			b.owner->SwapValues( owner, &b );
		}
	}
	std::swap( value, b.value );
	std::swap( exception, b.exception );
	std::swap( isReady, b.isReady );
}

template <typename T>
class __CFutureOwner {
private:
	bool created;
	CFuture<T>* value;

public:
	__CFutureOwner() : created( false ), value( 0 ) {}
	~__CFutureOwner()
	{
		std::lock_guard<std::mutex> guard( __FutureDestroyMutex );
		if( value != 0 ) {
			value->owner = 0;
		}
	}

	void Clear()
	{
		value = 0;
	}

	bool IsCreated()
	{
		return created;
	}

	bool HasValue()
	{
		return value != 0;
	}

	CFuture<T>* GetValue()
	{
		return value;
	}

	void SwapValues( __CFutureOwner<T>* other, CFuture<T>* newValue= 0 )
	{
		if( other != 0 ) {
			std::swap( value, other->value );
			std::swap( created, other->created );
		} else {
			value = newValue;
			created = newValue != 0;
		}
	}

	friend class CFuture<T>;
};
