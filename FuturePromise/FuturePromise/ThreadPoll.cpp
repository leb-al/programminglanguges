#include <exception>
#include "ThreadPoll.h"

class CStopException : public std::exception {

};

CThreadPool::CThreadPool( int workersNum ) : active( true ), activeThread( workersNum )
{
	for( int i = 0; i < workersNum; i++ ) {
		threads.push_back( std::thread( [this] { work(); } ) );
	}
}

CThreadPool::~CThreadPool()
{
	active = false;
	for( size_t i = 0; i < threads.size(); i++ ) {
		condition.notify_one();
		threads[i].join();
	}
}

void CThreadPool::AddTask( std::shared_ptr<CBasePromiseTask> task )
{
	addTask( task );
	condition.notify_one();
}

bool CThreadPool::Availiable()
{
	return activeThread < threads.size();
}

void CThreadPool::work()
{
	try {
		while( active ) {
			std::shared_ptr<CBasePromiseTask> task = getTask();
			(*task)();
		}
	}
	catch( CStopException& e ) {

	}
}

std::shared_ptr<CBasePromiseTask> CThreadPool::getTask()
{
	std::shared_ptr<CBasePromiseTask> task;
	std::unique_lock<std::mutex> lock( mutex );
	--activeThread;
	condition.wait( lock, [this] {
		return !testQueue.empty() || !active;
	} );
	if( active ) {
		++activeThread;
		task = testQueue.front();
		testQueue.pop();
		return task;
	} else {
		condition.notify_one();
		throw CStopException();
	}
}

void CThreadPool::addTask( std::shared_ptr<CBasePromiseTask> task )
{
	std::unique_lock<std::mutex> lock( mutex );
	testQueue.push( task );
}