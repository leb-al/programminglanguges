#pragma once
#include <thread>
#include <vector>
#include <mutex>
#include <queue>

#include "Promise.h"

class CBasePromiseTask {
public:
	void operator () ()
	{
		work();
	}
	virtual ~CBasePromiseTask() {}

protected:
	virtual void work() = 0;
};

template<typename T>
class CPromiseTask : public CBasePromiseTask {
public:
	CPromiseTask( std::function<T()> task ) : task( task )
	{
	}
	virtual ~CPromiseTask() {};

	CFuture<T> GetFuture()
	{
		return promise.GetFuture();
	}

protected:
	virtual void work() override
	{
		try {
			T result = task();
			promise.SetValue( result );
		}
		catch( std::exception* e ) {
			promise.SetException( e );
		}
	}

private:
	std::function<T()> task;
	CPromise<T> promise;
};

class CThreadPool {
public:

	CThreadPool( int workersNum );
	~CThreadPool();

	void AddTask( std::shared_ptr<CBasePromiseTask> task );
	bool Availiable();
private:
	std::queue<std::shared_ptr<CBasePromiseTask>> testQueue;
	std::mutex mutex;
	std::vector<std::thread> threads;
	std::condition_variable condition;
	bool active;
	int activeThread;

	void work();
	std::shared_ptr<CBasePromiseTask> getTask();
	void addTask( std::shared_ptr<CBasePromiseTask> task );
};