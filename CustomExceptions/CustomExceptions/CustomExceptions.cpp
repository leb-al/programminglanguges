// CustomExceptions.cpp: ���������� ����� ����� ��� ����������� ����������.

#include <iostream>

#include "Exception.h"
#include "TestClass.h"

#include "NonTrivial.h"

void throwFunc()
{
	THROW( new CNullPointerException( L"test" ) );
	THROW( new CException( L"test" ) );
}

int main()
{
	TRY
	{
		TRY
		{
			CTestClass tst( 7 );
			CNonTrivial<CTestClass> tst1( 1 );
			CNonTrivialObject<CTestClass> obj_( tst );
			{
				//CDestructorExceptionClass t(3);
			}
			//CDestructorExceptionClass tst5( 5 );
			//CNonTrivialObject<CDestructorExceptionClass> tst5_( tst5 );
			throwFunc();
		}
		CATCH( CNullPointerException, e )
		{
			std::wcerr << L"Handle nullptr: " << e->GetInformation() << std::endl;
			THROW( e );
		}
		CATCH( CException, e )
		{
			std::wcerr << L"Handle: " << e->GetInformation() << std::endl;
		}
		ENDTRYCATCH;
	}
	CATCH( CException, e )
	{
		std::wcerr << L"Handle root: " << e->GetInformation() << std::endl;
	}
	ENDTRYCATCH;

	return 0;
}
