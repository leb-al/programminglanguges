#pragma once
class CTestClass {
public:
	CTestClass( int value );
	virtual ~CTestClass();
	operator int();

private:
	int value;
};

//---------------------------------------------------------------------------------------------------------------------

class CDestructorExceptionClass : public CTestClass {
public:
	CDestructorExceptionClass( int value ) : CTestClass( value )
	{
	}
	virtual ~CDestructorExceptionClass();
};
