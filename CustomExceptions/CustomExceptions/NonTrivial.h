#pragma once

#include "Exception.h"

template <typename T>
class CNonTrivial : public T {
public:
	template <typename... Args>
	CNonTrivial( Args&&... params ) : T( std::forward<Args>( params )... ), holder( *this )
	{

	}
	virtual ~CNonTrivial() {}

private:
	CNonTrivialObject<T> holder;
};
