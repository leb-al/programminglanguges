#pragma once
#include <string>
#include <csetjmp>
#include <vector>

class CException {
public:
	inline CException( const std::wstring& _message ) : message( _message ), lineNum( 0 ), fileName( 0 )
	{

	}

	inline CException() : CException( L"" )
	{

	}

	virtual ~CException() {}

	std::wstring GetMessage();

	std::wstring GetInformation();

	void SetLocationInfo( int line, const char* file );

private:
	int lineNum;
	const char* fileName;
	std::wstring message;
};

//---------------------------------------------------------------------------------------------------------------------

class CNullPointerException : public CException {
public:
	CNullPointerException( const wchar_t* name ) : CException( std::wstring( name ) + L" is null" )
	{

	}
};

//---------------------------------------------------------------------------------------------------------------------

class __CTryPoint {
public:
	__CTryPoint();
	~__CTryPoint();

	jmp_buf JumpPoint;
	int SetJumpResult;
	CException* Exception;
	bool Handled;
	bool CatchRunning;

	void Close();
	__CTryPoint* GetLastTry();
private:
	__CTryPoint* last; // ������� try
	bool opened; // ��� ��������, ��� ��������� ������������ �������
};

//---------------------------------------------------------------------------------------------------------------------

class CDestructorHolder {
public:
	virtual ~CDestructorHolder() {}
	virtual void Destroy() = 0;

	static void StackUnwinding( const CDestructorHolder* reference );
};

//---------------------------------------------------------------------------------------------------------------------

struct __CExceptionInfo {
	__CTryPoint* LatestTry;
	CException* Exception;
	std::vector<CDestructorHolder*> DestructorHolders;

	__CExceptionInfo();
};

extern __CExceptionInfo __globalCExceptionInfo;

//---------------------------------------------------------------------------------------------------------------------

template <typename T>
class CNonTrivialObject : public CDestructorHolder {
public:
	inline CNonTrivialObject( T& object ) : pointer( &object )
	{
		__globalCExceptionInfo.DestructorHolders.push_back( this );
	}
	inline ~CNonTrivialObject()
	{
		for( int i = __globalCExceptionInfo.DestructorHolders.size() - 1; i >= 0; i-- ) {
			if( __globalCExceptionInfo.DestructorHolders[i] == this ) {
				__globalCExceptionInfo.DestructorHolders.erase( __globalCExceptionInfo.DestructorHolders.begin() + i );
				return;
			}
		}
		if( sizeof( size_t ) == 4 ) {
			std::terminate(); // �� ������ ���� ��������� � x86
		}
	}
	void Destroy()
	{
		pointer->~T();
	}

private:
	T* pointer;
};

//---------------------------------------------------------------------------------------------------------------------

void __ThrowException( CException* e, int line, const char* file );
void __ThrowException( CException* e );

#define THROW( e ) __ThrowException( e, __LINE__, __FILE__ );

#define TRY \
	{ \
		__CTryPoint __lastTry; \
		CNonTrivialObject<__CTryPoint> __lastTryNonTrivialReference( __lastTry ); \
		__lastTry.SetJumpResult = setjmp( __lastTry.JumpPoint ); \
		if( __lastTry.SetJumpResult != 0 ) { \
			__lastTry.CatchRunning = true; \
			__lastTry.Handled = false; \
			__lastTry.Exception = __globalCExceptionInfo.Exception; \
			CDestructorHolder::StackUnwinding( &__lastTryNonTrivialReference ); \
			__globalCExceptionInfo.Exception = 0; \
		} \
		if( __lastTry.SetJumpResult == 0 )

#define ENDTRYCATCH \
		__lastTry.Close(); \
	}

#define CATCH( T, e ) \
	{ \
		T* e; \
		if( ( e = dynamic_cast<T*>( __lastTry.Exception ) ) != 0 ) { \
			if (__lastTry.Handled ) { \
				delete __lastTry.Exception; \
				__lastTry.Exception = 0; \
			} else { \
				__lastTry.Handled = true; \
			} \
		} \
	} \
	if( T* e = dynamic_cast<T*>( __lastTry.Exception ) )
