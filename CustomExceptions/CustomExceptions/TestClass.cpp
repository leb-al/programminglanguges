#include <iostream>

#include "Exception.h"
#include "TestClass.h"

CTestClass::CTestClass( int _value ) : value( _value )
{
	std::cerr << "CTestClass ctor with " << value << std::endl;
}


CTestClass::~CTestClass()
{
	std::cerr << "~CTestClass dector for " << value << std::endl;
}

CTestClass::operator int()
{
	return value;
}

CDestructorExceptionClass::~CDestructorExceptionClass()
{
	THROW( new CException( L"For test" ) );
}
