#include <sstream>
#include <cassert>
#include <iostream>
#include "Exception.h"

static_assert(sizeof( size_t ) == 4, "Only x86 is supported");

std::wstring CException::GetMessage()
{
	return message;
}

std::wstring CException::GetInformation()
{
	std::wstringstream result;
	result << message;
	if( fileName != 0 ) {
		result << L" (" << fileName << L":" << lineNum << ")";
	}
	return result.str();
}

void CException::SetLocationInfo( int line, const char* file )
{
	lineNum = line;
	fileName = file;
}

//---------------------------------------------------------------------------------------------------------------------

void CDestructorHolder::StackUnwinding( const CDestructorHolder* reference )
{
	assert( __globalCExceptionInfo.Exception != 0 ); // ��� ������� ���������� ������ ��� ����������� ����������
	std::vector<CDestructorHolder*>& holders = __globalCExceptionInfo.DestructorHolders;
	while( !holders.empty() ) {
		CDestructorHolder* holder = holders.back();
		if( holder == reference ) {
			return;
		} else {
			holder->Destroy();
			if( holders.back() == holder ) {
				holders.pop_back();
			}
		}
	}
	std::terminate(); // �� ������ ���� �������� (�� ����� reference � holders);
}

//---------------------------------------------------------------------------------------------------------------------

__CExceptionInfo __globalCExceptionInfo;

//---------------------------------------------------------------------------------------------------------------------

__CTryPoint::__CTryPoint()
	: opened( true ), last( __globalCExceptionInfo.LatestTry ), Handled( true ), Exception( 0 ), CatchRunning( false )
{
	__globalCExceptionInfo.LatestTry = this;
}

__CTryPoint::~__CTryPoint()
{
	// ����� �������� �� ��, ��� ��������� ������� �������� (� �� ������ ��������)
	assert( !opened );
	if( opened ) {
		std::terminate(); // � VS ��� Release ��� ����������� ����������� assert �� ��������
	}
}

void __CTryPoint::Close()
{
	assert( opened );
	if( !opened ) {
		std::terminate(); // � VS ��� Release ��� ����������� ����������� assert �� ��������
	}

	opened = false;
	__globalCExceptionInfo.LatestTry = last;

	if( Handled ) {
		if( Exception != 0 ) {
			if( Exception != __globalCExceptionInfo.Exception ) {
				delete Exception;
			}
			Exception = 0;
		}
	} else {
		__ThrowException( Exception );
	}
}

__CTryPoint* __CTryPoint::GetLastTry()
{
	return last;
}

//---------------------------------------------------------------------------------------------------------------------

__CExceptionInfo::__CExceptionInfo() : LatestTry( 0 ), Exception( 0 )
{
}

//---------------------------------------------------------------------------------------------------------------------

void __ThrowException( CException* e, int line, const char* file )
{
	e->SetLocationInfo( line, file );
	__ThrowException( e );
}

void __ThrowException( CException* e )
{
	if( __globalCExceptionInfo.Exception != 0 ) {
		std::wcerr << L"Exception in stack unwinding " << e->GetInformation() << L" (have: " << __globalCExceptionInfo.Exception->GetInformation() << L")" << std::endl;
		std::terminate();
	}
	__globalCExceptionInfo.Exception = e;
	__CTryPoint* handler = __globalCExceptionInfo.LatestTry;
	while( handler != 0 ) {
		if( handler->CatchRunning ) {
			handler->Close();
			handler = handler->GetLastTry();
		} else {
			longjmp( handler->JumpPoint, 1 );
		}
	}
	std::wcerr << L"Unhandled excpetion: " << e->GetInformation();
	std::terminate();
}

